// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SpaceStationInfinity/SpaceStationInfinityGameModeBase.h"
#include "SpaceStationInfinity/Actors/Characters/Playable/PlayerCharacter.h"

class SPACESTATIONINFINITY_API GameHelper
{
public:
	static ASpaceStationInfinityGameModeBase* Mode(UObject* world);
};
