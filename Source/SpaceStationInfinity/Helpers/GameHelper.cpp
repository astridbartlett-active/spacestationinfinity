// Fill out your copyright notice in the Description page of Project Settings.


#include "GameHelper.h"

#include "Kismet/GameplayStatics.h"

ASpaceStationInfinityGameModeBase* GameHelper::Mode(UObject* world)
{
	return Cast<ASpaceStationInfinityGameModeBase>(UGameplayStatics::GetGameMode(world));
}
