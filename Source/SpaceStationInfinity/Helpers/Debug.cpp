// Fill out your copyright notice in the Description page of Project Settings.


#include "Debug.h"

uint64 Debug::_nextId = 1;
std::map<std::string, uint64> Debug::_keyMap = {};

void Debug::Screen(std::string key, FString message, float displayTime, FColor color)
{
    auto ikey = _keyMap.find(key);
    if(ikey == _keyMap.end()) 
    {
        ikey = _keyMap.insert({ key, _nextId++ }).first;
    }
    
    //GEngine->AddOnScreenDebugMessage(ikey->second, displayTime, color, message);
}
