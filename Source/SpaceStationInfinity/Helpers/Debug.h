// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <map>

/**
 * 
 */
class SPACESTATIONINFINITY_API Debug
{
private:
    static uint64 _nextId;
    static std::map<std::string, uint64> _keyMap;

public:
    static void Screen(std::string key, FString message, float displayTime = 5, FColor color = FColor::Red);

};
