// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "CutScenes/Conversation.h"
#include "GameFramework/GameModeBase.h"
#include "SpaceStationInfinityGameModeBase.generated.h"

class ULocalLightComponent;

UENUM(BlueprintType)
enum class CompletionStep : uint8 {
	WAKE_UP 		    UMETA(DisplayName = "WAKE UP"),
	INITIAL_EXPERIMENT 	UMETA(DisplayName = "INITIAL EXPERIMENT"),
	LOOP_WORLD 			UMETA(DisplayName = "LOOP WORLD"),
	QUANTUM_DAMPENERS	UMETA(DisplayName = "QUANTUM DAMPENERS"),
	READ_RIFT_BOOK		UMETA(DisplayName = "READ RIFT BOOK"),
	
};

UENUM(BlueprintType)
enum class CompletionType : uint8
{
	NOT_DONE	 	UMETA(DisplayName = "NOT DONE"),
	PREVIOUSLY		UMETA(DisplayName = "PREVIOUSLY"),
	COMPLETED_FIRST	UMETA(DisplayName = "COMPLETED FIRST"),
	COMPLETED	 	UMETA(DisplayName = "COMPLETED")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FListenForStateChange, CompletionStep, step, CompletionType, type);

struct StateChangeObserver
{
	FListenForStateChange callback;
	CompletionStep step;
	CompletionType type;
};

struct CurrentProgressData
{
	CompletionStep step;
	CompletionType type;

	CurrentProgressData()
	{
	}

	CurrentProgressData(CompletionStep _step)
	{
		step = _step;
		type = CompletionType::NOT_DONE;
	}
};

UCLASS()
class SPACESTATIONINFINITY_API ASpaceStationInfinityGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASpaceStationInfinityGameModeBase();
	~ASpaceStationInfinityGameModeBase();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	bool ShouldRunAi();

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void RegisterWarningLight(ULocalLightComponent* light);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void ActivateWarningLights(bool active);

	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void CompleteStep(CompletionStep step);
	
	UFUNCTION(BlueprintCallable, Category = "GameMode")
	void RestartDay();
	
	UFUNCTION(BlueprintCallable, Category = "GameMode")
	CompletionType GetStepState(CompletionStep step);

	UFUNCTION(BlueprintCallable, Category = "Conversation")
	UConversation* GetNewConversation(FString key);

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Conversation")
	UTexture2D* GetTexture(int index);

protected:
	void Notify(CompletionStep step, CompletionType type);
	
protected:

	UPROPERTY(EditAnywhere, Category = "GameMode")
	bool RunAi {true};

	UPROPERTY(EditAnywhere, Category = "GameMode|ConversationTextures")
	TArray<UTexture2D*> ConversationTextures;

	UPROPERTY(VisibleAnywhere, BlueprintAssignable, Category = "GameMode")
	FListenForStateChange OnStateChange;
	
	TArray<CurrentProgressData> CurrentProgress;

	bool UsingWarningLights {false};

	bool ConversationsReady {false};

	TArray<ULocalLightComponent*> WarningLights;
};
