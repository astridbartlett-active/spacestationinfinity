#pragma once

#include "CoreMinimal.h"
#include "Base/Enemy.h"
#include "Hunter.generated.h"

UCLASS()
class SPACESTATIONINFINITY_API AHunter : public AEnemy
{
	GENERATED_BODY()

public:
	AHunter();

	virtual void BeginPlay() override;
	virtual void AiTick(float deltaSeconds) override;

protected:
	
	UFUNCTION(BlueprintCallable, Category = "Hunter")
	void ShouldChasePlayer(bool should);

	bool ChasePlayer {false};
	
};
