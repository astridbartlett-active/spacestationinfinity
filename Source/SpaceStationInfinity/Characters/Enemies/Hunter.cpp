// Fill out your copyright notice in the Description page of Project Settings.


#include "Hunter.h"

#include "Kismet/GameplayStatics.h"
#include "SpaceStationInfinity/Actors/Characters/Playable/PlayerCharacter.h"
#include "SpaceStationInfinity/Actors/Environment/Checkpoint.h"
#include "SpaceStationInfinity/Actors/Interactable/Door.h"

AHunter::AHunter()
{
}

void AHunter::BeginPlay()
{
	Super::BeginPlay();
}


void AHunter::AiTick(float deltaSeconds)
{
	if (!ChasePlayer) return;
	
	auto player = Cast<APlayerCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if (player)
	{
		auto pCheck = player->GetCurrentCheckpoint();
		auto mCheck = this->GetCurrentCheckpoint();
		if(mCheck == pCheck)
		{
			auto dir = player->GetActorLocation() - GetActorLocation();
			dir.Normalize();
			AddMovementInput(dir);
		}
		else
		{
			int max = ACheckpoint::Max() + 1;
			if (pCheck < mCheck) pCheck += max;

			int forwardDist = pCheck - mCheck;
			int backDist = (mCheck + max) - pCheck;

			int dir = forwardDist < backDist ? 1 : -1;
			auto to = ADoor::NavigateThroughDoor(this, this->GetCurrentCheckpoint(), dir);

			auto towards = to - GetActorLocation();
			towards.Normalize();
			AddMovementInput(towards);
		}
	}

	Super::AiTick(deltaSeconds);
}

void AHunter::ShouldChasePlayer(bool should)
{
	ChasePlayer = should;
}
