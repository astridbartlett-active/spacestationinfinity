#include "Enemy.h"


#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "SpaceStationInfinity/Actors/Characters/Playable/PlayerCharacter.h"
#include "SpaceStationInfinity/Helpers/GameHelper.h"

AEnemy::AEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AEnemy::EnterCheckpoint(int order)
{
	if(CurrentCheckpoint != order)
	{
		CurrentCheckpoint = order;
		OnCheckpointChange(order);
	}
	
}

void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

void AEnemy::Tick(float deltaSeconds)
{
	if(GameHelper::Mode(GetWorld())->ShouldRunAi())
	{
		this->AiTick(deltaSeconds);
	}
	Super::Tick(deltaSeconds);
}

void AEnemy::EnterInteractArea(AInteractable* with)
{
	InteractWith = with;
}

void AEnemy::ExitInteractArea(AInteractable* with)
{
	if (InteractWith == with)
	{
		InteractWith = nullptr;
	}
}

bool AEnemy::CanInteractWith(AInteractable* with)
{
	return InteractWith == with;
}

int AEnemy::GetCurrentCheckpoint()
{
	return CurrentCheckpoint;
}

void AEnemy::AiTick(float deltaSeconds)
{
}

