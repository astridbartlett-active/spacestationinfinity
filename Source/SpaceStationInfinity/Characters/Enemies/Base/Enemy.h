// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

class AInteractable;
UCLASS()
class SPACESTATIONINFINITY_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	AEnemy();

	void EnterCheckpoint(int order);
	
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	void EnterInteractArea(AInteractable* with);

	void ExitInteractArea(AInteractable* with);

	bool CanInteractWith(AInteractable* with);

protected:
	
	UFUNCTION(BlueprintImplementableEvent, Category = "Enemy|Events")
	void OnCheckpointChange(int newCheckpoint);

	UFUNCTION(BlueprintCallable, Category = "Enemy")
	int GetCurrentCheckpoint();

	virtual void AiTick(float deltaSeconds);

protected:
	UPROPERTY(EditAnywhere, Category = "Enemy")
	int CurrentCheckpoint{ 0 };
	
	AInteractable* InteractWith;
};
