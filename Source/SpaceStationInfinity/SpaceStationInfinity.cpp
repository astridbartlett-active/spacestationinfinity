// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpaceStationInfinity.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SpaceStationInfinity, "SpaceStationInfinity" );
