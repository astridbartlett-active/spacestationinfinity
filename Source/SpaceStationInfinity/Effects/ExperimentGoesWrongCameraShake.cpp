// Fill out your copyright notice in the Description page of Project Settings.


#include "ExperimentGoesWrongCameraShake.h"

UExperimentGoesWrongCameraShake::UExperimentGoesWrongCameraShake()
{
    OscillationDuration = -1;
    OscillationBlendInTime = 12;
    OscillationBlendOutTime = 6;

    RotOscillation.Pitch.Amplitude = 1.5f;
    RotOscillation.Pitch.Frequency = 30;

    RotOscillation.Yaw.Amplitude = 1;
    RotOscillation.Yaw.Frequency = 20;

}
