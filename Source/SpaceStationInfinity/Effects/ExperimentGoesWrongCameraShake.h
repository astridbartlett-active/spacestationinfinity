// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "ExperimentGoesWrongCameraShake.generated.h"

/**
 * 
 */
UCLASS()
class SPACESTATIONINFINITY_API UExperimentGoesWrongCameraShake : public UCameraShake
{
	GENERATED_BODY()
	
public:
	UExperimentGoesWrongCameraShake();
};
