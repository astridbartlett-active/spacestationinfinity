#include "PlayerCharacter.h"


#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "SpaceStationInfinity/Actors/Interactable/Base/Interactable.h"
#include "SpaceStationInfinity/Helpers/Debug.h"

APlayerCharacter::APlayerCharacter()
{
	//PrimaryActorTick.bCanEverTick = true;

	//Create Default Objects
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));

	//Setup Default Objects
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400;
	CameraBoom->bUsePawnControlRotation = true;

	Camera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;

	//Other Things
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0, 500, 0);
	GetCharacterMovement()->JumpZVelocity = 600;
	GetCharacterMovement()->AirControl = 0.4;
	
	AutoPossessPlayer = EAutoReceiveInput::Player0;
}

void APlayerCharacter::BeginPlay()
{
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;

	if(MainHud && !Hud)
	{
		Hud = CreateWidget<UUserWidget>(GetPlayerController(), MainHud);
	}
	
	Super::BeginPlay();
}

void APlayerCharacter::EnterInteractArea(AInteractable* with)
{
	OnInteractableChange(InteractWith, with);
	InteractWith = with;
}

void APlayerCharacter::ExitInteractArea(AInteractable* with)
{
	if(InteractWith == with)
	{
		OnInteractableChange(InteractWith, nullptr);
		InteractWith = nullptr;
	}
}

void APlayerCharacter::UpdateInteractionData()
{
	if(InteractWith && !InteractWith->DoesAllowInteraction())
	{
		OnInteractableChange(InteractWith, nullptr);
	}
	else
	{
		OnInteractableChange(InteractWith, InteractWith);
	}
}

void APlayerCharacter::EnterCheckpoint(int order)
{
	if(this->LastCheckpoint != order)
	{
		this->LastCheckpoint = order;
		OnCheckpointChange(order);
		OnRoomChange.Broadcast(order);
	}
}

void APlayerCharacter::DebugInfo(FString message)
{
	Debug::Screen("bp_message", message);
}

void APlayerCharacter::MoveForward(float amount)
{
	Move(amount, EAxis::X);
}

void APlayerCharacter::MoveRight(float amount)
{
	Move(amount, EAxis::Y);
}

void APlayerCharacter::StartRunning()
{
	Running = true;
	GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
}

void APlayerCharacter::StopRunning()
{
	Running = false;
	GetCharacterMovement()->MaxWalkSpeed = WalkSpeed;
}

void APlayerCharacter::Freeze(bool freeze)
{
	CanMove = !freeze;
}

void APlayerCharacter::Move(float amount, EAxis::Type axis)
{
	if (!CanMove) return;
	
	auto rot = Controller->GetControlRotation();
	auto yaw = FRotator(0, rot.Yaw, 0);

	auto dir = FRotationMatrix(yaw).GetUnitAxis(axis);
	AddMovementInput(dir, amount);
}

APlayerController* APlayerCharacter::GetPlayerController()
{
	return Cast<APlayerController>(Controller);
}

void APlayerCharacter::TurnCamera(float rate)
{
	AddControllerYawInput(rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::LookUp(float rate)
{
	AddControllerPitchInput(rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::Interact()
{
	if(InteractWith && InteractWith->DoesAllowInteraction())
	{
		InteractWith->OnInteract(this);
	}
}

void APlayerCharacter::InitForPlay(bool removeAllWidgets)
{
	if(removeAllWidgets)
	{
		OnClearWidgets();
	}
	
	if(Hud && !Hud->IsInViewport())
	{
		Hud->AddToViewport();
	}

	auto pc = GetPlayerController();
	pc->bShowMouseCursor = false;
	pc->SetPause(false);
	UWidgetBlueprintLibrary::SetInputMode_GameOnly(pc);

	InMenu = false;
}

void APlayerCharacter::InitForMenu(UUserWidget* menu, bool removeOtherWidgets)
{
	if(removeOtherWidgets)
	{
		OnClearWidgets();
	}
	
	if (Hud && Hud->IsInViewport())
	{
		Hud->RemoveFromViewport();
	}

	if(menu && !menu->IsInViewport())
	{
		menu->AddToViewport();
	}

	auto pc = GetPlayerController();
	pc->bShowMouseCursor = true;
	pc->SetPause(true);
	UWidgetBlueprintLibrary::SetInputMode_GameAndUIEx(pc, menu);
	
	InMenu = true;
}

bool APlayerCharacter::IsInMenu()
{
	return InMenu;
}

UUserWidget* APlayerCharacter::GetHud()
{
	return Hud;
}

int APlayerCharacter::GetCurrentCheckpoint()
{
	return LastCheckpoint;
}

