// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraComponent.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"

#include "PlayerCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FChangeRoom, int, newRoom);

class AInteractable;
UCLASS()
class SPACESTATIONINFINITY_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

// Methods
public:
	APlayerCharacter();

	UFUNCTION(BlueprintCallable, Category = "Player|Events")
	void EnterInteractArea(AInteractable* with);
	
	UFUNCTION(BlueprintCallable, Category = "Player|Events")
	void ExitInteractArea(AInteractable* with);
	
	UFUNCTION(BlueprintCallable, Category = "Player|Events")
	void UpdateInteractionData();

	void EnterCheckpoint(int order);

	UFUNCTION(BlueprintCallable, Category = "Debug")
	static void DebugInfo(FString message);
	
	UFUNCTION(BlueprintCallable, Category = "Player")
	int GetCurrentCheckpoint();

//Methods
protected:
	virtual void BeginPlay() override;

	void Move(float amount, EAxis::Type axis);

	APlayerController* GetPlayerController();

	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	void MoveForward(float amount);

	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	void MoveRight(float amount);

	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	void StartRunning();

	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	void StopRunning();
	
	UFUNCTION(BlueprintCallable, Category = "Player|Movement")
	void Freeze(bool freeze);

	UFUNCTION(BlueprintCallable, Category = "Player|Camera")
	void TurnCamera(float amount);

	UFUNCTION(BlueprintCallable, Category = "Player|Camera")
	void LookUp(float amount);

	UFUNCTION(BlueprintImplementableEvent, Category = "Player|Events")
	void OnInteractableChange(AInteractable* from, AInteractable* to);

	UFUNCTION(BlueprintImplementableEvent, Category = "Player|Events")
	void OnClearWidgets();

	UFUNCTION(BlueprintImplementableEvent, Category = "Player|Events")
	void OnCheckpointChange(int newCheckpoint);
	
	UFUNCTION(BlueprintCallable, Category = "Player|Actions")
	void Interact();

	UFUNCTION(BlueprintCallable, Category = "Player|Menu")
	void InitForPlay(bool removeAllWidgets = true);
	
	UFUNCTION(BlueprintCallable, Category = "Player|Menu")
	void InitForMenu(UUserWidget* menu, bool removeOtherWidgets = true);

	UFUNCTION(BlueprintCallable, Category = "Player|Menu")
	bool IsInMenu();
	
	UFUNCTION(BlueprintCallable, Category = "Player|Menu")
	UUserWidget* GetHud();
	

public:
	UPROPERTY(VisibleAnywhere, BlueprintAssignable, Category = "Player|Events")
	FChangeRoom OnRoomChange;
	
//Properties
protected:

	UPROPERTY(EditAnywhere, Category = "Player|Camera")
	UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, Category = "Player|Camera")
	USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere, Category = "Player|Camera")
	float BaseTurnRate {45};

	UPROPERTY(EditAnywhere, Category = "Player|Camera")
	float BaseLookUpRate {45};
	
	UPROPERTY(EditAnywhere, Category = "Player|Movement")
	float WalkSpeed {1000};
	
	UPROPERTY(EditAnywhere, Category = "Player|Movement")
	float RunSpeed {2400};

	UPROPERTY(EditAnywhere, Category = "Player")
	TSubclassOf<UUserWidget> MainHud;

	UUserWidget* Hud;
	
	bool Running { false };

	bool CanMove { true };

	bool InMenu { false };

	AInteractable* InteractWith;

	int LastCheckpoint {0};
};
