#include "Checkpoint.h"

#include "Components/BoxComponent.h"
#include "SpaceStationInfinity/Actors/Characters/Playable/PlayerCharacter.h"
#include "SpaceStationInfinity/Characters/Enemies/Base/Enemy.h"
#include "SpaceStationInfinity/Helpers/Debug.h"

TArray<ACheckpoint*> ACheckpoint::_allCheckpoints = {};

ACheckpoint::ACheckpoint()
{
	Area = CreateDefaultSubobject<UBoxComponent>(TEXT("Area"));
	SetRootComponent(Area);
}

void ACheckpoint::BeginPlay()
{
	Area->SetGenerateOverlapEvents(true);

	Area->OnComponentBeginOverlap.AddDynamic(this, &ACheckpoint::BeginOverlap);
	Area->OnComponentEndOverlap.AddDynamic(this, &ACheckpoint::EndOverlap);

	RegisterCheckpoint(this);
	
	Super::BeginPlay();
}

void ACheckpoint::BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp,	int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	auto player = Cast<APlayerCharacter>(otherActor);
	if(player)
	{
		player->EnterCheckpoint(this->Order);
	}
	else
	{
		auto enemy = Cast<AEnemy>(otherActor);
		if(enemy)
		{
			enemy->EnterCheckpoint(this->Order);
		}
	}
}

void ACheckpoint::EndOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex)
{
}

void ACheckpoint::RegisterCheckpoint(ACheckpoint* checkpoint)
{
	_allCheckpoints.Add(checkpoint);
}

void ACheckpoint::FinalizeCheckpoints()
{
	_allCheckpoints.Sort(&ACheckpoint::CheckOrder);
}

bool ACheckpoint::CheckOrder(ACheckpoint& first, ACheckpoint& second)
{
	return first.Order < second.Order;
}

int ACheckpoint::Max()
{
	return _allCheckpoints[_allCheckpoints.Num() - 1]->Order;
}

int ACheckpoint::FindNext(int current, int direction)
{
	int max = _allCheckpoints[_allCheckpoints.Num() - 1]->Order;
	if(direction < 0)
	{
		return current <= 0 ? max : current - 1;
	}
	else
	{
		return current >= max ? 0 : current + 1;
	}
}
