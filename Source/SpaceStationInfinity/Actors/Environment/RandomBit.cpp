#include "RandomBit.h"

ARandomBit::ARandomBit()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	BaseMesh->SetupAttachment(Root);
	
	AvailableMeshes = {};
}

void ARandomBit::BeginPlay()
{
	auto mesh = AvailableMeshes[rand() % AvailableMeshes.Num()];
	auto scaleAmount = ScaleMin >= ScaleMax ? 1 : ((rand() / double(RAND_MAX)) * (ScaleMax - ScaleMin)) + ScaleMin;
	auto rotAmount = RotMin >= RotMax ? 0 : ((rand() / double(RAND_MAX)) * (RotMax - RotMin)) + RotMin;
	
	BaseMesh->SetStaticMesh(mesh);
	Root->SetRelativeScale3D(FVector(scaleAmount, scaleAmount, scaleAmount));
	Root->SetRelativeRotation(FRotator(0, rotAmount, 0));
	
	Super::BeginPlay();
}

