// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RandomBit.generated.h"

UCLASS()
class SPACESTATIONINFINITY_API ARandomBit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARandomBit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, Category = "Random Bit")
	USceneComponent* Root;
	
	UPROPERTY(EditAnywhere, Category = "Random Bit")
	UStaticMeshComponent* BaseMesh;

	UPROPERTY(EditAnywhere, Category = "Random Bit")
	TArray<UStaticMesh*> AvailableMeshes;

	UPROPERTY(EditAnywhere, Category = "Random Bit")
	float ScaleMin {0.9f};
	
	UPROPERTY(EditAnywhere, Category = "Random Bit")
	float ScaleMax {1.1f};

	UPROPERTY(EditAnywhere, Category = "Random Bit")
	int RotMin {0};
	
	UPROPERTY(EditAnywhere, Category = "Random Bit")
	int RotMax {359};

public:	

};
