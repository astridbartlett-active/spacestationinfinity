#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Checkpoint.generated.h"

class UBoxComponent;
UCLASS()
class SPACESTATIONINFINITY_API ACheckpoint : public AActor
{
	GENERATED_BODY()
	
public:	
	ACheckpoint();
	
	UFUNCTION(BlueprintCallable, Category = "Checkpoints")
	static void FinalizeCheckpoints();

	static int FindNext(int current, int direction);
	
	static bool CheckOrder(ACheckpoint& first, ACheckpoint& second);

	static int Max();
	
protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult);

	UFUNCTION()
	virtual void EndOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex);

	static void RegisterCheckpoint(ACheckpoint* checkpoint);
	
protected:
	UPROPERTY(EditAnywhere, Category = "Checkpoint")
	UBoxComponent* Area;

	UPROPERTY(EditAnywhere, Category = "Checkpoint")
	int Order {0};
	
	static TArray<ACheckpoint*> _allCheckpoints;
};
