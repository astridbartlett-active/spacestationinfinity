// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Base/InteractableSphere.h"
#include "Door.generated.h"

class AEnemy;

UCLASS()
class SPACESTATIONINFINITY_API ADoor : public AInteractableSphere
{
	GENERATED_BODY()

public:
	ADoor();

	virtual void BeginPlay() override;
	static FVector NavigateThroughDoor(AEnemy* navigating, int section, int direction);

protected:

	static void RegisterDoor(ADoor* door);

public:

	UPROPERTY(EditAnywhere, Category = "Door")
	FVector AiPoint1;

	UPROPERTY(EditAnywhere, Category = "Door")
	FVector AiPoint2;
	
protected:

	UPROPERTY(EditAnywhere, Category = "Door")
	int Section1 {0};
	
	UPROPERTY(EditAnywhere, Category = "Door")
	int Section2 {0};

	UPROPERTY(BlueprintReadWrite, Category = "Door")
	bool IsOpen {false};
	
	static TArray<ADoor*> _allDoors;
};
