// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableSphere.h"

#include "Components/SphereComponent.h"

AInteractableSphere::AInteractableSphere()
{
	InteractableArea = CreateDefaultSubobject<USphereComponent>(TEXT("InteractableArea"));
	InteractableArea->SetupAttachment(RootComponent);
}

void AInteractableSphere::BeginPlay()
{
	Super::BeginPlay();
	if(InteractableArea)
	{
		InteractableArea->SetGenerateOverlapEvents(true);

		InteractableArea->OnComponentBeginOverlap.AddDynamic(this, &AInteractableSphere::BeginOverlap);
		InteractableArea->OnComponentEndOverlap.AddDynamic(this, &AInteractableSphere::EndOverlap);
	}
}
