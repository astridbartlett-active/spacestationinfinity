#include "Interactable.h"

#include "SpaceStationInfinity/Characters/Enemies/Base/Enemy.h"

// Sets default values
AInteractable::AInteractable()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	InteractionDescription = TEXT("Interact");
}

FString AInteractable::GetInteractionDescription()
{
	return InteractionDescription;
}

bool AInteractable::DoesAllowInteraction()
{
	return CanInteractWith;
}

void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

void AInteractable::BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	if(!CanInteractWith)
	{
		return;
	}
	auto player = Cast<APlayerCharacter>(otherActor);
	if(player)
	{
		player->EnterInteractArea(this);
	}
	else
	{
		auto enemy = Cast<AEnemy>(otherActor);
		if(enemy)
		{
			enemy->EnterInteractArea(this);
		}
	}
}

void AInteractable::EndOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp,	int32 otherBodyIndex)
{
	auto player = Cast<APlayerCharacter>(otherActor);
	if (player)
	{
		player->ExitInteractArea(this);
	}
	else
	{
		auto enemy = Cast<AEnemy>(otherActor);
		if (enemy)
		{
			enemy->ExitInteractArea(this);
		}
	}
}

