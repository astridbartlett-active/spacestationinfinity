// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "InteractableBox.generated.h"

class UBoxComponent;
/**
 * 
 */
UCLASS()
class SPACESTATIONINFINITY_API AInteractableBox : public AInteractable
{
	GENERATED_BODY()

public:
	AInteractableBox();

	virtual void BeginPlay() override;
	
protected:

	UPROPERTY(EditAnywhere, Category = "Interactable")
	UBoxComponent* InteractableArea;
};
