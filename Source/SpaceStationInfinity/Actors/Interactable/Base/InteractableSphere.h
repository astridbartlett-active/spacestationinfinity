// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "InteractableSphere.generated.h"

class USphereComponent;
/**
 * 
 */
UCLASS()
class SPACESTATIONINFINITY_API AInteractableSphere : public AInteractable
{
	GENERATED_BODY()

public:
	AInteractableSphere();

	virtual void BeginPlay() override;
	
protected:

	UPROPERTY(EditAnywhere, Category = "Interactable")
	USphereComponent* InteractableArea;
};
