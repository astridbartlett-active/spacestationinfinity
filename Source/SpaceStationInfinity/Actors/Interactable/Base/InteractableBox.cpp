// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableBox.h"

#include "Components/BoxComponent.h"

AInteractableBox::AInteractableBox()
{
	InteractableArea = CreateDefaultSubobject<UBoxComponent>(TEXT("InteractableArea"));
	InteractableArea->SetupAttachment(RootComponent);
}

void AInteractableBox::BeginPlay()
{
	Super::BeginPlay();
	if (InteractableArea)
	{
		InteractableArea->SetGenerateOverlapEvents(true);

		InteractableArea->OnComponentBeginOverlap.AddDynamic(this, &AInteractableBox::BeginOverlap);
		InteractableArea->OnComponentEndOverlap.AddDynamic(this, &AInteractableBox::EndOverlap);
	}
}