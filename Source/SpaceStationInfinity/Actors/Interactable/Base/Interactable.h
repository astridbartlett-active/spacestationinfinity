#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../../Characters/Playable/PlayerCharacter.h"
#include "Interactable.generated.h"

UCLASS()
class SPACESTATIONINFINITY_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	AInteractable();

	UFUNCTION(BlueprintImplementableEvent, Category = "Interactable|Events")
	void OnInteract(AActor* with);

	UFUNCTION(BlueprintCallable, Category = "Interactable")
	FString GetInteractionDescription();

	bool DoesAllowInteraction();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	virtual void BeginOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult);

	UFUNCTION()
	virtual void EndOverlap(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 otherBodyIndex);

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Interactable")
	USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable")
	FString InteractionDescription;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable")
	bool CanInteractWith {true};

};
