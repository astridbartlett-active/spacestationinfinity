// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"

#include "SpaceStationInfinity/Actors/Environment/Checkpoint.h"
#include "SpaceStationInfinity/Characters/Enemies/Base/Enemy.h"

TArray<ADoor*> ADoor::_allDoors = {};

ADoor::ADoor()
{
}

void ADoor::BeginPlay()
{
	RegisterDoor(this);
	Super::BeginPlay();
}

void ADoor::RegisterDoor(ADoor* door)
{
	_allDoors.Add(door);
}

FVector ADoor::NavigateThroughDoor(AEnemy* navigating, int section, int direction)
{
	int next = ACheckpoint::FindNext(section, direction);
	for(int i = 0; i < _allDoors.Num(); i++)
	{
		auto door = _allDoors[i];
		if((door->Section1 == section && door->Section2 == next) || (door->Section2 == section && door->Section1 == next))
		{
			if(!door->IsOpen && navigating->CanInteractWith(door))
			{
				door->OnInteract(navigating);
			}

			FVector rel;
			
			if(door->IsOpen)
			{
				rel = direction > 0 ? door->AiPoint1 : door->AiPoint2;
			}
			else
			{
				rel = direction > 0 ? door->AiPoint2 : door->AiPoint1;
			}

			return door->GetActorLocation() + rel;
		}
	}

	return navigating->GetActorLocation();
}
