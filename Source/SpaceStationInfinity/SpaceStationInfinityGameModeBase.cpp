// Copyright Epic Games, Inc. All Rights Reserved.


#include "SpaceStationInfinityGameModeBase.h"

#include "Components/LocalLightComponent.h"
#include "CutScenes/Conversation.h"

ASpaceStationInfinityGameModeBase::ASpaceStationInfinityGameModeBase()
{
	WarningLights = {};
	CurrentProgress.Add(CurrentProgressData(CompletionStep::WAKE_UP));
	CurrentProgress.Add(CurrentProgressData(CompletionStep::INITIAL_EXPERIMENT));
	CurrentProgress.Add(CurrentProgressData(CompletionStep::LOOP_WORLD));
	CurrentProgress.Add(CurrentProgressData(CompletionStep::QUANTUM_DAMPENERS));
	CurrentProgress.Add(CurrentProgressData(CompletionStep::READ_RIFT_BOOK));
}

ASpaceStationInfinityGameModeBase::~ASpaceStationInfinityGameModeBase()
{
}

bool ASpaceStationInfinityGameModeBase::ShouldRunAi()
{
	return RunAi;
}

void ASpaceStationInfinityGameModeBase::RegisterWarningLight(ULocalLightComponent* light)
{
	WarningLights.Add(light);
}

void ASpaceStationInfinityGameModeBase::ActivateWarningLights(bool active)
{
	if (active == UsingWarningLights) return;

	UsingWarningLights = active;

	for(int i = 0; i < WarningLights.Num(); i++)
	{
		WarningLights[i]->SetLightColor(UsingWarningLights ? FLinearColor::Red : FLinearColor::White);
	}
}

void ASpaceStationInfinityGameModeBase::CompleteStep(CompletionStep step)
{
	for(int i = 0; i < CurrentProgress.Num(); i++)
	{
		if(CurrentProgress[i].step == step)
		{
			if(CurrentProgress[i].type == CompletionType::NOT_DONE)
			{
				Notify(CurrentProgress[i].step, CompletionType::COMPLETED_FIRST);
			}
			CurrentProgress[i].type = CompletionType::COMPLETED;
			Notify(CurrentProgress[i].step, CurrentProgress[i].type);
		}
	}
}

void ASpaceStationInfinityGameModeBase::RestartDay()
{
	for (int i = 0; i < CurrentProgress.Num(); i++)
	{
		if (CurrentProgress[i].type == CompletionType::COMPLETED)
		{
			CurrentProgress[i].type = CompletionType::PREVIOUSLY;
			Notify(CurrentProgress[i].step, CurrentProgress[i].type);
		}
	}
}

CompletionType ASpaceStationInfinityGameModeBase::GetStepState(CompletionStep step)
{
	for(int i = 0; i < CurrentProgress.Num(); i++)
	{
		if (CurrentProgress[i].step == step) return CurrentProgress[i].type;
	}

	return CompletionType::NOT_DONE;
}

UConversation* ASpaceStationInfinityGameModeBase::GetNewConversation(FString key)
{
	auto mic = 0;
	auto assistant = 1;
	auto assistantDead = 2;
	auto scienceMan = 3;

	if(key == "wake_up")
	{
		auto wakeUp = NewObject<UConversation>();
		wakeUp->Add(new FConversationItemLocal("Doctor, we're all ready to go with the experiment", 3, mic));
		wakeUp->Add(new FConversationItemLocal("We'll be waiting in the science lab when you're ready", 3, mic));
		wakeUp->Init();

		return wakeUp;
	}
	else if(key == "initial_experiment")
	{
		auto initial_experiment = NewObject<UConversation>();
		initial_experiment->Add(new FConversationItemLocal("Initializing...", 3, assistant));
		initial_experiment->Add(new FConversationItemLocal("Doctor... it's working! The Ludumus Darenite is stabilizing. You'll win a Nobel prize for sure for synthesizing a new kind of exotic matter.", 10, assistant));
		initial_experiment->Add(new FConversationItemLocal("Wait a second... these readings are a bit off", 5, assistant));
		initial_experiment->Add(new FConversationItemLocal("Oh No... Spacial Fractures are forming inside the chambers... And something's coming out of them!", 7, assistant));
		initial_experiment->Add(new FConversationItemLocal("What's going on! What are those things?!?!? Doctor get out of here! I'm activating a red alert!", 10, assistant));
		initial_experiment->Add(new FConversationItemLocal("... ... ...", 5, assistantDead));
		initial_experiment->Init();

		return initial_experiment;
	}
	else if (key == "assistant_pre_experiment_talk")
	{
		auto assistant_pre_experiment_talk = NewObject<UConversation>();
		assistant_pre_experiment_talk->Add(new FConversationItemLocal("Just go up to the machine to start the experiment", 3, assistant));
		assistant_pre_experiment_talk->Init();

		return assistant_pre_experiment_talk;
	}
	else if (key == "player_change_quantum_dampeners")
	{
		auto player_change_quantum_dampeners = NewObject<UConversation>();
		player_change_quantum_dampeners->Add(new FConversationItemLocal("Quantum Dampeners set, not that it really did anything last time", 5, scienceMan));
		player_change_quantum_dampeners->Init();

		return player_change_quantum_dampeners;
	}
	else if (key == "player_change_quantum_dampeners_first")
	{
		auto player_change_quantum_dampeners_first = NewObject<UConversation>();
		player_change_quantum_dampeners_first->Add(new FConversationItemLocal("Quantum Dampeners all set, hope this works. Guess I'll head to sleep and see tomorrow.", 6, scienceMan));
		player_change_quantum_dampeners_first->Init();

		return player_change_quantum_dampeners_first;
	}
	else if (key == "assistant_first_loop")
	{
		auto assistant_first_loop = NewObject<UConversation>();
		assistant_first_loop->Add(new FConversationItemLocal("Hey! Ready for the big day?", 3, assistant));
		assistant_first_loop->Add(new FConversationItemLocal("What? Red Alert? No, I'm fine, I'm definitely still alive...", 5, assistant));
		assistant_first_loop->Add(new FConversationItemLocal("You're sounding crazy. When was the last time you slept? Two or Three days ago maybe?", 5, assistant));
		assistant_first_loop->Add(new FConversationItemLocal("You're not making any sense, your experiment is scheduled for tomorrow, not this morning.", 6, assistant));
		assistant_first_loop->Add(new FConversationItemLocal("What?! No! You can't just cancel the experiment, Billions of dollars have been spent on it, you'll make a lot of people angry if you don't go through with it.", 7, assistant));
		assistant_first_loop->Add(new FConversationItemLocal("If you're really that worried about it activate the quantum dampeners on the station, that should prevent anything like what you're talking about.", 7, assistant));
		assistant_first_loop->Init();

		return assistant_first_loop;
	}
	else if (key == "assistant_after_quantum_dampen")
	{
		auto assistant_after_quantum_dampen = NewObject<UConversation>();
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("Hey! Ready for the big day?", 3, assistant));
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("What, you're stuck in a loop? I'm not sure I follow.", 5, assistant));
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("Have you been sleeping well?", 2, assistant));
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("Alright! Fine! I'll take your word for it. No need to get all worked up about it.", 5, assistant));
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("Try turning on the quantum dampeners, that should prevent...", 5, assistant));
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("Oh... you already tried that. Well temporal anomalies like that are fairly rare. Only a few have been actually documented.", 8, assistant));
		assistant_after_quantum_dampen->Add(new FConversationItemLocal("Maybe do some research and see how others handled it?", 5, assistant));
		assistant_after_quantum_dampen->Init();

		return assistant_after_quantum_dampen;
	}
	else if (key == "player_research")
	{
		auto player_research = NewObject<UConversation>();
		player_research->Add(new FConversationItemLocal("Hmm, there are some notes in here. Maybe I can use these calculations to adjust my experiment.", 7, scienceMan));
		player_research->Add(new FConversationItemLocal("Guess I'll have to wait until next time to see if this works.", 5, scienceMan));
		player_research->Init();

		return player_research;
	}
	else if (key == "experiment_after_research")
	{
		auto experiment_after_research = NewObject<UConversation>();
		experiment_after_research->Add(new FConversationItemLocal("Notes for some final adjustments? Alright, I'll enter these in.", 4, assistant));
		experiment_after_research->Add(new FConversationItemLocal("That should do it. Initializing...", 4, assistant));
		experiment_after_research->Add(new FConversationItemLocal("Doctor... it's working! The Ludumus Darenite is stabilizing. You'll win a Nobel prize for sure for synthesizing a new kind of exotic matter.", 10, assistant));
		experiment_after_research->Add(new FConversationItemLocal("Wait a second... these readings are a bit off", 5, assistant));
		experiment_after_research->Add(new FConversationItemLocal("Just joking around. Everything seems alright", 8, assistant));
		experiment_after_research->Add(new FConversationItemLocal("Congratulations doctor!", 6, assistant));
		experiment_after_research->Init();
		
		return experiment_after_research;
	}

	return nullptr;
}

void ASpaceStationInfinityGameModeBase::BeginPlay()
{
}

UTexture2D* ASpaceStationInfinityGameModeBase::GetTexture(int index)
{
	return ConversationTextures[index];
}

void ASpaceStationInfinityGameModeBase::Notify(CompletionStep step, CompletionType type)
{
	OnStateChange.Broadcast(step, type);
}
