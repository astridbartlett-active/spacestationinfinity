// Fill out your copyright notice in the Description page of Project Settings.


#include "Conversation.h"


#include "SpaceStationInfinity/Helpers/Debug.h"
#include "SpaceStationInfinity/Helpers/GameHelper.h"

UConversation::UConversation()
{
	
}

void UConversation::Reset()
{
	Index = 0;
}

bool UConversation::IsFinished()
{
	return Index >= actualItems;
}

FConversationItem UConversation::Next()
{
	if (IsFinished()) return FConversationItem();

	OnConversationProgress.Broadcast(Index);
	FConversationItemLocal* next = nullptr;
	switch(Index)
	{
		case 0: next = Item0; break;
		case 1: next = Item1; break;
		case 2: next = Item2; break;
		case 3: next = Item3; break;
		case 4: next = Item4; break;
		case 5: next = Item5; break;
		case 6: next = Item6; break;
		case 7: next = Item7; break;
		case 8: next = Item8; break;
		case 9: next = Item9; break;
		default: Debug::Screen("too_many_conversation_items", "Too Many Conversation Items"); return FConversationItem();
	}

	if(!next)
	{
		Debug::Screen("bad_item", "Item doesn't exist");
		return FConversationItem();
	}

	Index++;

	auto text = next->Text;
	auto durr = next->Duration;
	auto image = next->ImageIndex;
	
	return FConversationItem(text, durr, image);
}

void UConversation::Add(FConversationItemLocal* item)
{
	switch (nextItem)
	{
		case 0: Item0 = item; break;
		case 1: Item1 = item; break;
		case 2: Item2 = item; break;
		case 3: Item3 = item; break;
		case 4: Item4 = item; break;
		case 5: Item5 = item; break;
		case 6: Item6 = item; break;
		case 7: Item7 = item; break;
		case 8: Item8 = item; break;
		case 9: Item9 = item; break;
		default: Debug::Screen("too_many_conversation_items", "Too Many Conversation Items"); return;
	}
	actualItems++;
	nextItem++;
}

void UConversation::Init()
{
}
