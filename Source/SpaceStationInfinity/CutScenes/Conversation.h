// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include <map>

#include "Conversation.generated.h"

struct FConversationItemLocal
{
	const char* Text;
	float Duration;
	int ImageIndex;

	FConversationItemLocal()
	{
	}

	FConversationItemLocal(const char* text, float durration, int image)
	{
		Text = text;
		Duration = durration;
		ImageIndex = image;
	}
};

USTRUCT(BlueprintType)
struct FConversationItem
{
	GENERATED_BODY();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Conversation")
	FString Text;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Conversation")
	float Duration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Conversation")
	int ImageIndex;

	FConversationItem()
	{
		Text = "";
		Duration = 0;
		ImageIndex = -1;
	}

	FConversationItem(FString text, float durration, int image)
	{
		Text = text;
		Duration = durration;
		ImageIndex = image;
	}
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FConversationCallback, int, stepNumber);

UCLASS(BlueprintType)
class SPACESTATIONINFINITY_API UConversation : public UObject
{
	GENERATED_BODY()
	
public:
	UConversation();

	UFUNCTION(BlueprintCallable, Category = "Conversation")
	void Reset();
	
	UFUNCTION(BlueprintCallable, Category = "Conversation")
	bool IsFinished();
	
	UFUNCTION(BlueprintCallable, Category = "Conversation")
	FConversationItem Next();

	void Add(FConversationItemLocal* item);
	
	void Init();

public:

	UPROPERTY(VisibleAnywhere, BlueprintAssignable, Category = "Conversation")
	FConversationCallback OnConversationProgress;
	
protected:
	int Index {0};
	int actualItems {0};
	int nextItem {0};
	
	FConversationItemLocal* Item0;
	FConversationItemLocal* Item1;
	FConversationItemLocal* Item2;
	FConversationItemLocal* Item3;
	FConversationItemLocal* Item4;
	FConversationItemLocal* Item5;
	FConversationItemLocal* Item6;
	FConversationItemLocal* Item7;
	FConversationItemLocal* Item8;
	FConversationItemLocal* Item9;
};
